Name:           dwarfs
Version:        0.10.2
Release:        1%{?dist}
Summary:        A fast high compression read-only file system

License:        GPLv3
URL:            https://github.com/mhx/%{name}
Source0:        https://github.com/mhx/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.xz

Requires: boost-chrono
Requires: boost-iostreams
Requires: boost-program-options
Requires: bzip2-libs
Requires: double-conversion
Requires: fmt
Requires: gflags
Requires: glog
Requires: jemalloc
Requires: libacl
Requires: libarchive
Requires: libattr
Requires: libbrotli
Requires: libstdc++
Requires: libunwind
Requires: libxml2
Requires: libzstd
Requires: lz4-libs
Requires: openssl-libs
Requires: xxhash-libs
Requires: xz-libs
Requires: zlib

BuildRequires: binutils-devel
BuildRequires: bison
BuildRequires: boost-chrono
BuildRequires: boost-context
BuildRequires: boost-devel
BuildRequires: boost-filesystem
BuildRequires: boost-iostreams
BuildRequires: boost-program-options
BuildRequires: boost-regex
BuildRequires: boost-system
BuildRequires: boost-thread
BuildRequires: brotli-devel
BuildRequires: ccache
BuildRequires: clang
BuildRequires: cmake
BuildRequires: date-devel
BuildRequires: double-conversion-devel
BuildRequires: elfutils-devel
BuildRequires: file-devel
BuildRequires: flac-devel
BuildRequires: flex
BuildRequires: fmt-devel
BuildRequires: fuse3
BuildRequires: fuse3-devel
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: git
BuildRequires: glog-devel
BuildRequires: gmock-devel
BuildRequires: google-benchmark-devel
BuildRequires: gtest-devel 
BuildRequires: jemalloc-devel
BuildRequires: json-devel
BuildRequires: libacl-devel
BuildRequires: libarchive-devel
BuildRequires: libdwarf-devel
BuildRequires: libevent-devel
BuildRequires: libunwind-devel
BuildRequires: lz4-devel
BuildRequires: make
BuildRequires: ninja-build
BuildRequires: openssl-devel
BuildRequires: pkgconf
BuildRequires: range-v3-devel
BuildRequires: rubygem-ronn-ng
BuildRequires: utf8cpp-devel
BuildRequires: xxhash-devel
BuildRequires: xz-devel

%description
DwarFS is a read-only file system with a focus on achieving very high compression ratios in particular for very redundant data.

%prep
%autosetup
sed -i '/utils_test/d' CMakeLists.txt

%build
%cmake -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
-DBUILD_SHARED_LIBS=ON \
-DWITH_MAN_OPTION=OFF \
%cmake_build

%install
%cmake_install
### HACK

%files
%doc
%{_mandir}/man5/dwarfs-format.5*
%{_mandir}/man1/mkdwarfs.1*
%{_mandir}/man1/dwarfsck.1*
%{_mandir}/man1/dwarfsextract.1*
%{_mandir}/man1/dwarfs.1*

%{_libdir}/libdwarfs_common.so
%{_libdir}/libdwarfs_common.so.%{version}
%{_libdir}/libdwarfs_reader.so
%{_libdir}/libdwarfs_reader.so.%{version}
%{_libdir}/libdwarfs_writer.so
%{_libdir}/libdwarfs_writer.so.%{version}
%{_libdir}/libdwarfs_extractor.so
%{_libdir}/libdwarfs_extractor.so.%{version}
%{_libdir}/libdwarfs_rewrite.so
%{_libdir}/libdwarfs_rewrite.so.%{version}

%{_includedir}/dwarfs/
%{_includedir}/dwarfs/writer/entry_filter.h
%{_includedir}/dwarfs/writer/object.h
%{_includedir}/dwarfs/writer/entry_factory.h
%{_includedir}/dwarfs/writer/entry_transformer.h
%{_includedir}/dwarfs/writer/segmenter.h
%{_includedir}/dwarfs/writer/console_writer.h
%{_includedir}/dwarfs/writer/segmenter_factory.h
%{_includedir}/dwarfs/writer/fragment_order_parser.h
%{_includedir}/dwarfs/writer/scanner.h
%{_includedir}/dwarfs/writer/writer_progress.h
%{_includedir}/dwarfs/writer/category_resolver.h
%{_includedir}/dwarfs/writer/filter_debug.h
%{_includedir}/dwarfs/writer/categorizer.h
%{_includedir}/dwarfs/writer/filesystem_writer_options.h
%{_includedir}/dwarfs/writer/categorized_option.h
%{_includedir}/dwarfs/writer/rule_based_entry_filter.h
%{_includedir}/dwarfs/writer/fragment_category.h
%{_includedir}/dwarfs/writer/entry_interface.h
%{_includedir}/dwarfs/writer/filesystem_writer.h
%{_includedir}/dwarfs/writer/fragment_order_options.h
%{_includedir}/dwarfs/writer/scanner_options.h
%{_includedir}/dwarfs/writer/chmod_entry_transformer.h
%{_includedir}/dwarfs/writer/inode_options.h
%{_includedir}/dwarfs/writer/inode_fragments.h
%{_includedir}/dwarfs/writer/category_parser.h
%{_includedir}/dwarfs/writer/compression_metadata_requirements.h
%{_includedir}/dwarfs/writer/filesystem_block_category_resolver.h
%{_includedir}/dwarfs/writer/contextual_option.h
%{_includedir}/dwarfs/utility/filesystem_extractor.h
%{_includedir}/dwarfs/utility/rewrite_options.h
%{_includedir}/dwarfs/utility/rewrite_filesystem.h
%{_includedir}/dwarfs/reader/fsinfo_features.h
%{_includedir}/dwarfs/reader/inode_reader_options.h
%{_includedir}/dwarfs/reader/getattr_options.h
%{_includedir}/dwarfs/reader/filesystem_options.h
%{_includedir}/dwarfs/reader/block_cache_options.h
%{_includedir}/dwarfs/reader/metadata_types.h
%{_includedir}/dwarfs/reader/mlock_mode.h
%{_includedir}/dwarfs/reader/fsinfo_options.h
%{_includedir}/dwarfs/reader/block_range.h
%{_includedir}/dwarfs/reader/iovec_read_buf.h
%{_includedir}/dwarfs/reader/metadata_options.h
%{_includedir}/dwarfs/reader/filesystem_v2.h
%{_includedir}/dwarfs/reader/cache_tidy_config.h
%{_includedir}/dwarfs/scope_exit.h
%{_includedir}/dwarfs/file_stat.h
%{_includedir}/dwarfs/compression.h
%{_includedir}/dwarfs/integral_value_parser.h
%{_includedir}/dwarfs/vfs_stat.h
%{_includedir}/dwarfs/terminal.h
%{_includedir}/dwarfs/terminal_ansi.h
%{_includedir}/dwarfs/history.h
%{_includedir}/dwarfs/xattr.h
%{_includedir}/dwarfs/mmap.h
%{_includedir}/dwarfs/thread_pool.h
%{_includedir}/dwarfs/file_util.h
%{_includedir}/dwarfs/format.h
%{_includedir}/dwarfs/fstypes.h
%{_includedir}/dwarfs/logger.h
%{_includedir}/dwarfs/history_config.h
%{_includedir}/dwarfs/util.h
%{_includedir}/dwarfs/zstd_context_manager.h
%{_includedir}/dwarfs/pcm_sample_transformer.h
%{_includedir}/dwarfs/os_access_generic.h
%{_includedir}/dwarfs/map_util.h
%{_includedir}/dwarfs/compiler.h
%{_includedir}/dwarfs/checksum.h
%{_includedir}/dwarfs/block_compressor.h
%{_includedir}/dwarfs/compression_constraints.h
%{_includedir}/dwarfs/library_dependencies.h
%{_includedir}/dwarfs/file_access.h
%{_includedir}/dwarfs/mmif.h
%{_includedir}/dwarfs/performance_monitor.h
%{_includedir}/dwarfs/file_type.h
%{_includedir}/dwarfs/file_access_generic.h
%{_includedir}/dwarfs/small_vector.h
%{_includedir}/dwarfs/conv.h
%{_includedir}/dwarfs/types.h
%{_includedir}/dwarfs/match.h
%{_includedir}/dwarfs/string.h
%{_includedir}/dwarfs/os_access.h
%{_includedir}/dwarfs/option_map.h
%{_includedir}/dwarfs/error.h
%{_includedir}/dwarfs/version.h
%{_includedir}/dwarfs/block_compressor_parser.h
%{_includedir}/dwarfs/config.h

%{_libdir}/cmake/dwarfs/dwarfs-config.cmake
%{_libdir}/cmake/dwarfs/dwarfs-config-version.cmake
%{_libdir}/cmake/dwarfs/dwarfs-targets.cmake
%{_libdir}/cmake/dwarfs/dwarfs-targets-release.cmake

%{_bindir}/mkdwarfs
%{_bindir}/dwarfsck
%{_bindir}/dwarfsextract
%{_sbindir}/mount.dwarfs
%{_sbindir}/dwarfs

%changelog
* Fri Jan 12 2024 silentnoodle - 0.7.4-1
- rewritten spec
